package com.github.data.response;

import com.github.data.dto.User;
import com.github.data.ws.response.BaseNameResponse;
import com.google.gson.Gson;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.annotation.Config;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertNotNull;

@RunWith(RobolectricTestRunner.class)
@Config(manifest = Config.NONE)
public class UserResponseTest {
    private Gson gson = new Gson();

    @Test
    public void testGetNamesUsers() {
        String answer = "[\n" +
                "  {\n" +
                "    \"login\": \"mojombo\",\n" +
                "    \"id\": 1,\n" +
                "    \"avatar_url\": \"https://avatars0.githubusercontent.com/u/1?v=4\",\n" +
                "    \"gravatar_id\": \"\",\n" +
                "    \"url\": \"https://api.github.com/users/mojombo\",\n" +
                "    \"html_url\": \"https://github.com/mojombo\",\n" +
                "    \"followers_url\": \"https://api.github.com/users/mojombo/followers\",\n" +
                "    \"following_url\": \"https://api.github.com/users/mojombo/following{/other_user}\",\n" +
                "    \"gists_url\": \"https://api.github.com/users/mojombo/gists{/gist_id}\",\n" +
                "    \"starred_url\": \"https://api.github.com/users/mojombo/starred{/owner}{/repo}\",\n" +
                "    \"subscriptions_url\": \"https://api.github.com/users/mojombo/subscriptions\",\n" +
                "    \"organizations_url\": \"https://api.github.com/users/mojombo/orgs\",\n" +
                "    \"repos_url\": \"https://api.github.com/users/mojombo/repos\",\n" +
                "    \"events_url\": \"https://api.github.com/users/mojombo/events{/privacy}\",\n" +
                "    \"received_events_url\": \"https://api.github.com/users/mojombo/received_events\",\n" +
                "    \"type\": \"User\",\n" +
                "    \"site_admin\": false\n" +
                "  },\n" +
                "  {\n" +
                "    \"login\": \"defunkt\",\n" +
                "    \"id\": 2,\n" +
                "    \"avatar_url\": \"https://avatars0.githubusercontent.com/u/2?v=4\",\n" +
                "    \"gravatar_id\": \"\",\n" +
                "    \"url\": \"https://api.github.com/users/defunkt\",\n" +
                "    \"html_url\": \"https://github.com/defunkt\",\n" +
                "    \"followers_url\": \"https://api.github.com/users/defunkt/followers\",\n" +
                "    \"following_url\": \"https://api.github.com/users/defunkt/following{/other_user}\",\n" +
                "    \"gists_url\": \"https://api.github.com/users/defunkt/gists{/gist_id}\",\n" +
                "    \"starred_url\": \"https://api.github.com/users/defunkt/starred{/owner}{/repo}\",\n" +
                "    \"subscriptions_url\": \"https://api.github.com/users/defunkt/subscriptions\",\n" +
                "    \"organizations_url\": \"https://api.github.com/users/defunkt/orgs\",\n" +
                "    \"repos_url\": \"https://api.github.com/users/defunkt/repos\",\n" +
                "    \"events_url\": \"https://api.github.com/users/defunkt/events{/privacy}\",\n" +
                "    \"received_events_url\": \"https://api.github.com/users/defunkt/received_events\",\n" +
                "    \"type\": \"User\",\n" +
                "    \"site_admin\": true\n" +
                "  },\n" +
                "  {\n" +
                "    \"login\": \"pjhyett\",\n" +
                "    \"id\": 3,\n" +
                "    \"avatar_url\": \"https://avatars0.githubusercontent.com/u/3?v=4\",\n" +
                "    \"gravatar_id\": \"\",\n" +
                "    \"url\": \"https://api.github.com/users/pjhyett\",\n" +
                "    \"html_url\": \"https://github.com/pjhyett\",\n" +
                "    \"followers_url\": \"https://api.github.com/users/pjhyett/followers\",\n" +
                "    \"following_url\": \"https://api.github.com/users/pjhyett/following{/other_user}\",\n" +
                "    \"gists_url\": \"https://api.github.com/users/pjhyett/gists{/gist_id}\",\n" +
                "    \"starred_url\": \"https://api.github.com/users/pjhyett/starred{/owner}{/repo}\",\n" +
                "    \"subscriptions_url\": \"https://api.github.com/users/pjhyett/subscriptions\",\n" +
                "    \"organizations_url\": \"https://api.github.com/users/pjhyett/orgs\",\n" +
                "    \"repos_url\": \"https://api.github.com/users/pjhyett/repos\",\n" +
                "    \"events_url\": \"https://api.github.com/users/pjhyett/events{/privacy}\",\n" +
                "    \"received_events_url\": \"https://api.github.com/users/pjhyett/received_events\",\n" +
                "    \"type\": \"User\",\n" +
                "    \"site_admin\": false\n" +
                "  },\n" +
                "  {\n" +
                "    \"login\": \"wycats\",\n" +
                "    \"id\": 4,\n" +
                "    \"avatar_url\": \"https://avatars0.githubusercontent.com/u/4?v=4\",\n" +
                "    \"gravatar_id\": \"\",\n" +
                "    \"url\": \"https://api.github.com/users/wycats\",\n" +
                "    \"html_url\": \"https://github.com/wycats\",\n" +
                "    \"followers_url\": \"https://api.github.com/users/wycats/followers\",\n" +
                "    \"following_url\": \"https://api.github.com/users/wycats/following{/other_user}\",\n" +
                "    \"gists_url\": \"https://api.github.com/users/wycats/gists{/gist_id}\",\n" +
                "    \"starred_url\": \"https://api.github.com/users/wycats/starred{/owner}{/repo}\",\n" +
                "    \"subscriptions_url\": \"https://api.github.com/users/wycats/subscriptions\",\n" +
                "    \"organizations_url\": \"https://api.github.com/users/wycats/orgs\",\n" +
                "    \"repos_url\": \"https://api.github.com/users/wycats/repos\",\n" +
                "    \"events_url\": \"https://api.github.com/users/wycats/events{/privacy}\",\n" +
                "    \"received_events_url\": \"https://api.github.com/users/wycats/received_events\",\n" +
                "    \"type\": \"User\",\n" +
                "    \"site_admin\": false\n" +
                "  },\n" +
                "  {\n" +
                "    \"login\": \"ezmobius\",\n" +
                "    \"id\": 5,\n" +
                "    \"avatar_url\": \"https://avatars0.githubusercontent.com/u/5?v=4\",\n" +
                "    \"gravatar_id\": \"\",\n" +
                "    \"url\": \"https://api.github.com/users/ezmobius\",\n" +
                "    \"html_url\": \"https://github.com/ezmobius\",\n" +
                "    \"followers_url\": \"https://api.github.com/users/ezmobius/followers\",\n" +
                "    \"following_url\": \"https://api.github.com/users/ezmobius/following{/other_user}\",\n" +
                "    \"gists_url\": \"https://api.github.com/users/ezmobius/gists{/gist_id}\",\n" +
                "    \"starred_url\": \"https://api.github.com/users/ezmobius/starred{/owner}{/repo}\",\n" +
                "    \"subscriptions_url\": \"https://api.github.com/users/ezmobius/subscriptions\",\n" +
                "    \"organizations_url\": \"https://api.github.com/users/ezmobius/orgs\",\n" +
                "    \"repos_url\": \"https://api.github.com/users/ezmobius/repos\",\n" +
                "    \"events_url\": \"https://api.github.com/users/ezmobius/events{/privacy}\",\n" +
                "    \"received_events_url\": \"https://api.github.com/users/ezmobius/received_events\",\n" +
                "    \"type\": \"User\",\n" +
                "    \"site_admin\": false\n" +
                "  },\n" +
                "  {\n" +
                "    \"login\": \"ivey\",\n" +
                "    \"id\": 6,\n" +
                "    \"avatar_url\": \"https://avatars0.githubusercontent.com/u/6?v=4\",\n" +
                "    \"gravatar_id\": \"\",\n" +
                "    \"url\": \"https://api.github.com/users/ivey\",\n" +
                "    \"html_url\": \"https://github.com/ivey\",\n" +
                "    \"followers_url\": \"https://api.github.com/users/ivey/followers\",\n" +
                "    \"following_url\": \"https://api.github.com/users/ivey/following{/other_user}\",\n" +
                "    \"gists_url\": \"https://api.github.com/users/ivey/gists{/gist_id}\",\n" +
                "    \"starred_url\": \"https://api.github.com/users/ivey/starred{/owner}{/repo}\",\n" +
                "    \"subscriptions_url\": \"https://api.github.com/users/ivey/subscriptions\",\n" +
                "    \"organizations_url\": \"https://api.github.com/users/ivey/orgs\",\n" +
                "    \"repos_url\": \"https://api.github.com/users/ivey/repos\",\n" +
                "    \"events_url\": \"https://api.github.com/users/ivey/events{/privacy}\",\n" +
                "    \"received_events_url\": \"https://api.github.com/users/ivey/received_events\",\n" +
                "    \"type\": \"User\",\n" +
                "    \"site_admin\": false\n" +
                "  },\n" +
                "  {\n" +
                "    \"login\": \"evanphx\",\n" +
                "    \"id\": 7,\n" +
                "    \"avatar_url\": \"https://avatars0.githubusercontent.com/u/7?v=4\",\n" +
                "    \"gravatar_id\": \"\",\n" +
                "    \"url\": \"https://api.github.com/users/evanphx\",\n" +
                "    \"html_url\": \"https://github.com/evanphx\",\n" +
                "    \"followers_url\": \"https://api.github.com/users/evanphx/followers\",\n" +
                "    \"following_url\": \"https://api.github.com/users/evanphx/following{/other_user}\",\n" +
                "    \"gists_url\": \"https://api.github.com/users/evanphx/gists{/gist_id}\",\n" +
                "    \"starred_url\": \"https://api.github.com/users/evanphx/starred{/owner}{/repo}\",\n" +
                "    \"subscriptions_url\": \"https://api.github.com/users/evanphx/subscriptions\",\n" +
                "    \"organizations_url\": \"https://api.github.com/users/evanphx/orgs\",\n" +
                "    \"repos_url\": \"https://api.github.com/users/evanphx/repos\",\n" +
                "    \"events_url\": \"https://api.github.com/users/evanphx/events{/privacy}\",\n" +
                "    \"received_events_url\": \"https://api.github.com/users/evanphx/received_events\",\n" +
                "    \"type\": \"User\",\n" +
                "    \"site_admin\": false\n" +
                "  },\n" +
                "  {\n" +
                "    \"login\": \"vanpelt\",\n" +
                "    \"id\": 17,\n" +
                "    \"avatar_url\": \"https://avatars1.githubusercontent.com/u/17?v=4\",\n" +
                "    \"gravatar_id\": \"\",\n" +
                "    \"url\": \"https://api.github.com/users/vanpelt\",\n" +
                "    \"html_url\": \"https://github.com/vanpelt\",\n" +
                "    \"followers_url\": \"https://api.github.com/users/vanpelt/followers\",\n" +
                "    \"following_url\": \"https://api.github.com/users/vanpelt/following{/other_user}\",\n" +
                "    \"gists_url\": \"https://api.github.com/users/vanpelt/gists{/gist_id}\",\n" +
                "    \"starred_url\": \"https://api.github.com/users/vanpelt/starred{/owner}{/repo}\",\n" +
                "    \"subscriptions_url\": \"https://api.github.com/users/vanpelt/subscriptions\",\n" +
                "    \"organizations_url\": \"https://api.github.com/users/vanpelt/orgs\",\n" +
                "    \"repos_url\": \"https://api.github.com/users/vanpelt/repos\",\n" +
                "    \"events_url\": \"https://api.github.com/users/vanpelt/events{/privacy}\",\n" +
                "    \"received_events_url\": \"https://api.github.com/users/vanpelt/received_events\",\n" +
                "    \"type\": \"User\",\n" +
                "    \"site_admin\": false\n" +
                "  },\n" +
                "  {\n" +
                "    \"login\": \"wayneeseguin\",\n" +
                "    \"id\": 18,\n" +
                "    \"avatar_url\": \"https://avatars0.githubusercontent.com/u/18?v=4\",\n" +
                "    \"gravatar_id\": \"\",\n" +
                "    \"url\": \"https://api.github.com/users/wayneeseguin\",\n" +
                "    \"html_url\": \"https://github.com/wayneeseguin\",\n" +
                "    \"followers_url\": \"https://api.github.com/users/wayneeseguin/followers\",\n" +
                "    \"following_url\": \"https://api.github.com/users/wayneeseguin/following{/other_user}\",\n" +
                "    \"gists_url\": \"https://api.github.com/users/wayneeseguin/gists{/gist_id}\",\n" +
                "    \"starred_url\": \"https://api.github.com/users/wayneeseguin/starred{/owner}{/repo}\",\n" +
                "    \"subscriptions_url\": \"https://api.github.com/users/wayneeseguin/subscriptions\",\n" +
                "    \"organizations_url\": \"https://api.github.com/users/wayneeseguin/orgs\",\n" +
                "    \"repos_url\": \"https://api.github.com/users/wayneeseguin/repos\",\n" +
                "    \"events_url\": \"https://api.github.com/users/wayneeseguin/events{/privacy}\",\n" +
                "    \"received_events_url\": \"https://api.github.com/users/wayneeseguin/received_events\",\n" +
                "    \"type\": \"User\",\n" +
                "    \"site_admin\": false\n" +
                "  },\n" +
                "  {\n" +
                "    \"login\": \"brynary\",\n" +
                "    \"id\": 19,\n" +
                "    \"avatar_url\": \"https://avatars0.githubusercontent.com/u/19?v=4\",\n" +
                "    \"gravatar_id\": \"\",\n" +
                "    \"url\": \"https://api.github.com/users/brynary\",\n" +
                "    \"html_url\": \"https://github.com/brynary\",\n" +
                "    \"followers_url\": \"https://api.github.com/users/brynary/followers\",\n" +
                "    \"following_url\": \"https://api.github.com/users/brynary/following{/other_user}\",\n" +
                "    \"gists_url\": \"https://api.github.com/users/brynary/gists{/gist_id}\",\n" +
                "    \"starred_url\": \"https://api.github.com/users/brynary/starred{/owner}{/repo}\",\n" +
                "    \"subscriptions_url\": \"https://api.github.com/users/brynary/subscriptions\",\n" +
                "    \"organizations_url\": \"https://api.github.com/users/brynary/orgs\",\n" +
                "    \"repos_url\": \"https://api.github.com/users/brynary/repos\",\n" +
                "    \"events_url\": \"https://api.github.com/users/brynary/events{/privacy}\",\n" +
                "    \"received_events_url\": \"https://api.github.com/users/brynary/received_events\",\n" +
                "    \"type\": \"User\",\n" +
                "    \"site_admin\": false\n" +
                "  },\n" +
                "  {\n" +
                "    \"login\": \"kevinclark\",\n" +
                "    \"id\": 20,\n" +
                "    \"avatar_url\": \"https://avatars3.githubusercontent.com/u/20?v=4\",\n" +
                "    \"gravatar_id\": \"\",\n" +
                "    \"url\": \"https://api.github.com/users/kevinclark\",\n" +
                "    \"html_url\": \"https://github.com/kevinclark\",\n" +
                "    \"followers_url\": \"https://api.github.com/users/kevinclark/followers\",\n" +
                "    \"following_url\": \"https://api.github.com/users/kevinclark/following{/other_user}\",\n" +
                "    \"gists_url\": \"https://api.github.com/users/kevinclark/gists{/gist_id}\",\n" +
                "    \"starred_url\": \"https://api.github.com/users/kevinclark/starred{/owner}{/repo}\",\n" +
                "    \"subscriptions_url\": \"https://api.github.com/users/kevinclark/subscriptions\",\n" +
                "    \"organizations_url\": \"https://api.github.com/users/kevinclark/orgs\",\n" +
                "    \"repos_url\": \"https://api.github.com/users/kevinclark/repos\",\n" +
                "    \"events_url\": \"https://api.github.com/users/kevinclark/events{/privacy}\",\n" +
                "    \"received_events_url\": \"https://api.github.com/users/kevinclark/received_events\",\n" +
                "    \"type\": \"User\",\n" +
                "    \"site_admin\": false\n" +
                "  },\n" +
                "  {\n" +
                "    \"login\": \"technoweenie\",\n" +
                "    \"id\": 21,\n" +
                "    \"avatar_url\": \"https://avatars3.githubusercontent.com/u/21?v=4\",\n" +
                "    \"gravatar_id\": \"\",\n" +
                "    \"url\": \"https://api.github.com/users/technoweenie\",\n" +
                "    \"html_url\": \"https://github.com/technoweenie\",\n" +
                "    \"followers_url\": \"https://api.github.com/users/technoweenie/followers\",\n" +
                "    \"following_url\": \"https://api.github.com/users/technoweenie/following{/other_user}\",\n" +
                "    \"gists_url\": \"https://api.github.com/users/technoweenie/gists{/gist_id}\",\n" +
                "    \"starred_url\": \"https://api.github.com/users/technoweenie/starred{/owner}{/repo}\",\n" +
                "    \"subscriptions_url\": \"https://api.github.com/users/technoweenie/subscriptions\",\n" +
                "    \"organizations_url\": \"https://api.github.com/users/technoweenie/orgs\",\n" +
                "    \"repos_url\": \"https://api.github.com/users/technoweenie/repos\",\n" +
                "    \"events_url\": \"https://api.github.com/users/technoweenie/events{/privacy}\",\n" +
                "    \"received_events_url\": \"https://api.github.com/users/technoweenie/received_events\",\n" +
                "    \"type\": \"User\",\n" +
                "    \"site_admin\": true\n" +
                "  },\n" +
                "  {\n" +
                "    \"login\": \"macournoyer\",\n" +
                "    \"id\": 22,\n" +
                "    \"avatar_url\": \"https://avatars3.githubusercontent.com/u/22?v=4\",\n" +
                "    \"gravatar_id\": \"\",\n" +
                "    \"url\": \"https://api.github.com/users/macournoyer\",\n" +
                "    \"html_url\": \"https://github.com/macournoyer\",\n" +
                "    \"followers_url\": \"https://api.github.com/users/macournoyer/followers\",\n" +
                "    \"following_url\": \"https://api.github.com/users/macournoyer/following{/other_user}\",\n" +
                "    \"gists_url\": \"https://api.github.com/users/macournoyer/gists{/gist_id}\",\n" +
                "    \"starred_url\": \"https://api.github.com/users/macournoyer/starred{/owner}{/repo}\",\n" +
                "    \"subscriptions_url\": \"https://api.github.com/users/macournoyer/subscriptions\",\n" +
                "    \"organizations_url\": \"https://api.github.com/users/macournoyer/orgs\",\n" +
                "    \"repos_url\": \"https://api.github.com/users/macournoyer/repos\",\n" +
                "    \"events_url\": \"https://api.github.com/users/macournoyer/events{/privacy}\",\n" +
                "    \"received_events_url\": \"https://api.github.com/users/macournoyer/received_events\",\n" +
                "    \"type\": \"User\",\n" +
                "    \"site_admin\": false\n" +
                "  },\n" +
                "  {\n" +
                "    \"login\": \"takeo\",\n" +
                "    \"id\": 23,\n" +
                "    \"avatar_url\": \"https://avatars3.githubusercontent.com/u/23?v=4\",\n" +
                "    \"gravatar_id\": \"\",\n" +
                "    \"url\": \"https://api.github.com/users/takeo\",\n" +
                "    \"html_url\": \"https://github.com/takeo\",\n" +
                "    \"followers_url\": \"https://api.github.com/users/takeo/followers\",\n" +
                "    \"following_url\": \"https://api.github.com/users/takeo/following{/other_user}\",\n" +
                "    \"gists_url\": \"https://api.github.com/users/takeo/gists{/gist_id}\",\n" +
                "    \"starred_url\": \"https://api.github.com/users/takeo/starred{/owner}{/repo}\",\n" +
                "    \"subscriptions_url\": \"https://api.github.com/users/takeo/subscriptions\",\n" +
                "    \"organizations_url\": \"https://api.github.com/users/takeo/orgs\",\n" +
                "    \"repos_url\": \"https://api.github.com/users/takeo/repos\",\n" +
                "    \"events_url\": \"https://api.github.com/users/takeo/events{/privacy}\",\n" +
                "    \"received_events_url\": \"https://api.github.com/users/takeo/received_events\",\n" +
                "    \"type\": \"User\",\n" +
                "    \"site_admin\": false\n" +
                "  },\n" +
                "  {\n" +
                "    \"login\": \"Caged\",\n" +
                "    \"id\": 25,\n" +
                "    \"avatar_url\": \"https://avatars3.githubusercontent.com/u/25?v=4\",\n" +
                "    \"gravatar_id\": \"\",\n" +
                "    \"url\": \"https://api.github.com/users/Caged\",\n" +
                "    \"html_url\": \"https://github.com/Caged\",\n" +
                "    \"followers_url\": \"https://api.github.com/users/Caged/followers\",\n" +
                "    \"following_url\": \"https://api.github.com/users/Caged/following{/other_user}\",\n" +
                "    \"gists_url\": \"https://api.github.com/users/Caged/gists{/gist_id}\",\n" +
                "    \"starred_url\": \"https://api.github.com/users/Caged/starred{/owner}{/repo}\",\n" +
                "    \"subscriptions_url\": \"https://api.github.com/users/Caged/subscriptions\",\n" +
                "    \"organizations_url\": \"https://api.github.com/users/Caged/orgs\",\n" +
                "    \"repos_url\": \"https://api.github.com/users/Caged/repos\",\n" +
                "    \"events_url\": \"https://api.github.com/users/Caged/events{/privacy}\",\n" +
                "    \"received_events_url\": \"https://api.github.com/users/Caged/received_events\",\n" +
                "    \"type\": \"User\",\n" +
                "    \"site_admin\": true\n" +
                "  },\n" +
                "  {\n" +
                "    \"login\": \"topfunky\",\n" +
                "    \"id\": 26,\n" +
                "    \"avatar_url\": \"https://avatars3.githubusercontent.com/u/26?v=4\",\n" +
                "    \"gravatar_id\": \"\",\n" +
                "    \"url\": \"https://api.github.com/users/topfunky\",\n" +
                "    \"html_url\": \"https://github.com/topfunky\",\n" +
                "    \"followers_url\": \"https://api.github.com/users/topfunky/followers\",\n" +
                "    \"following_url\": \"https://api.github.com/users/topfunky/following{/other_user}\",\n" +
                "    \"gists_url\": \"https://api.github.com/users/topfunky/gists{/gist_id}\",\n" +
                "    \"starred_url\": \"https://api.github.com/users/topfunky/starred{/owner}{/repo}\",\n" +
                "    \"subscriptions_url\": \"https://api.github.com/users/topfunky/subscriptions\",\n" +
                "    \"organizations_url\": \"https://api.github.com/users/topfunky/orgs\",\n" +
                "    \"repos_url\": \"https://api.github.com/users/topfunky/repos\",\n" +
                "    \"events_url\": \"https://api.github.com/users/topfunky/events{/privacy}\",\n" +
                "    \"received_events_url\": \"https://api.github.com/users/topfunky/received_events\",\n" +
                "    \"type\": \"User\",\n" +
                "    \"site_admin\": false\n" +
                "  },\n" +
                "  {\n" +
                "    \"login\": \"anotherjesse\",\n" +
                "    \"id\": 27,\n" +
                "    \"avatar_url\": \"https://avatars3.githubusercontent.com/u/27?v=4\",\n" +
                "    \"gravatar_id\": \"\",\n" +
                "    \"url\": \"https://api.github.com/users/anotherjesse\",\n" +
                "    \"html_url\": \"https://github.com/anotherjesse\",\n" +
                "    \"followers_url\": \"https://api.github.com/users/anotherjesse/followers\",\n" +
                "    \"following_url\": \"https://api.github.com/users/anotherjesse/following{/other_user}\",\n" +
                "    \"gists_url\": \"https://api.github.com/users/anotherjesse/gists{/gist_id}\",\n" +
                "    \"starred_url\": \"https://api.github.com/users/anotherjesse/starred{/owner}{/repo}\",\n" +
                "    \"subscriptions_url\": \"https://api.github.com/users/anotherjesse/subscriptions\",\n" +
                "    \"organizations_url\": \"https://api.github.com/users/anotherjesse/orgs\",\n" +
                "    \"repos_url\": \"https://api.github.com/users/anotherjesse/repos\",\n" +
                "    \"events_url\": \"https://api.github.com/users/anotherjesse/events{/privacy}\",\n" +
                "    \"received_events_url\": \"https://api.github.com/users/anotherjesse/received_events\",\n" +
                "    \"type\": \"User\",\n" +
                "    \"site_admin\": false\n" +
                "  },\n" +
                "  {\n" +
                "    \"login\": \"roland\",\n" +
                "    \"id\": 28,\n" +
                "    \"avatar_url\": \"https://avatars2.githubusercontent.com/u/28?v=4\",\n" +
                "    \"gravatar_id\": \"\",\n" +
                "    \"url\": \"https://api.github.com/users/roland\",\n" +
                "    \"html_url\": \"https://github.com/roland\",\n" +
                "    \"followers_url\": \"https://api.github.com/users/roland/followers\",\n" +
                "    \"following_url\": \"https://api.github.com/users/roland/following{/other_user}\",\n" +
                "    \"gists_url\": \"https://api.github.com/users/roland/gists{/gist_id}\",\n" +
                "    \"starred_url\": \"https://api.github.com/users/roland/starred{/owner}{/repo}\",\n" +
                "    \"subscriptions_url\": \"https://api.github.com/users/roland/subscriptions\",\n" +
                "    \"organizations_url\": \"https://api.github.com/users/roland/orgs\",\n" +
                "    \"repos_url\": \"https://api.github.com/users/roland/repos\",\n" +
                "    \"events_url\": \"https://api.github.com/users/roland/events{/privacy}\",\n" +
                "    \"received_events_url\": \"https://api.github.com/users/roland/received_events\",\n" +
                "    \"type\": \"User\",\n" +
                "    \"site_admin\": false\n" +
                "  },\n" +
                "  {\n" +
                "    \"login\": \"lukas\",\n" +
                "    \"id\": 29,\n" +
                "    \"avatar_url\": \"https://avatars2.githubusercontent.com/u/29?v=4\",\n" +
                "    \"gravatar_id\": \"\",\n" +
                "    \"url\": \"https://api.github.com/users/lukas\",\n" +
                "    \"html_url\": \"https://github.com/lukas\",\n" +
                "    \"followers_url\": \"https://api.github.com/users/lukas/followers\",\n" +
                "    \"following_url\": \"https://api.github.com/users/lukas/following{/other_user}\",\n" +
                "    \"gists_url\": \"https://api.github.com/users/lukas/gists{/gist_id}\",\n" +
                "    \"starred_url\": \"https://api.github.com/users/lukas/starred{/owner}{/repo}\",\n" +
                "    \"subscriptions_url\": \"https://api.github.com/users/lukas/subscriptions\",\n" +
                "    \"organizations_url\": \"https://api.github.com/users/lukas/orgs\",\n" +
                "    \"repos_url\": \"https://api.github.com/users/lukas/repos\",\n" +
                "    \"events_url\": \"https://api.github.com/users/lukas/events{/privacy}\",\n" +
                "    \"received_events_url\": \"https://api.github.com/users/lukas/received_events\",\n" +
                "    \"type\": \"User\",\n" +
                "    \"site_admin\": false\n" +
                "  },\n" +
                "  {\n" +
                "    \"login\": \"fanvsfan\",\n" +
                "    \"id\": 30,\n" +
                "    \"avatar_url\": \"https://avatars2.githubusercontent.com/u/30?v=4\",\n" +
                "    \"gravatar_id\": \"\",\n" +
                "    \"url\": \"https://api.github.com/users/fanvsfan\",\n" +
                "    \"html_url\": \"https://github.com/fanvsfan\",\n" +
                "    \"followers_url\": \"https://api.github.com/users/fanvsfan/followers\",\n" +
                "    \"following_url\": \"https://api.github.com/users/fanvsfan/following{/other_user}\",\n" +
                "    \"gists_url\": \"https://api.github.com/users/fanvsfan/gists{/gist_id}\",\n" +
                "    \"starred_url\": \"https://api.github.com/users/fanvsfan/starred{/owner}{/repo}\",\n" +
                "    \"subscriptions_url\": \"https://api.github.com/users/fanvsfan/subscriptions\",\n" +
                "    \"organizations_url\": \"https://api.github.com/users/fanvsfan/orgs\",\n" +
                "    \"repos_url\": \"https://api.github.com/users/fanvsfan/repos\",\n" +
                "    \"events_url\": \"https://api.github.com/users/fanvsfan/events{/privacy}\",\n" +
                "    \"received_events_url\": \"https://api.github.com/users/fanvsfan/received_events\",\n" +
                "    \"type\": \"User\",\n" +
                "    \"site_admin\": false\n" +
                "  },\n" +
                "  {\n" +
                "    \"login\": \"tomtt\",\n" +
                "    \"id\": 31,\n" +
                "    \"avatar_url\": \"https://avatars2.githubusercontent.com/u/31?v=4\",\n" +
                "    \"gravatar_id\": \"\",\n" +
                "    \"url\": \"https://api.github.com/users/tomtt\",\n" +
                "    \"html_url\": \"https://github.com/tomtt\",\n" +
                "    \"followers_url\": \"https://api.github.com/users/tomtt/followers\",\n" +
                "    \"following_url\": \"https://api.github.com/users/tomtt/following{/other_user}\",\n" +
                "    \"gists_url\": \"https://api.github.com/users/tomtt/gists{/gist_id}\",\n" +
                "    \"starred_url\": \"https://api.github.com/users/tomtt/starred{/owner}{/repo}\",\n" +
                "    \"subscriptions_url\": \"https://api.github.com/users/tomtt/subscriptions\",\n" +
                "    \"organizations_url\": \"https://api.github.com/users/tomtt/orgs\",\n" +
                "    \"repos_url\": \"https://api.github.com/users/tomtt/repos\",\n" +
                "    \"events_url\": \"https://api.github.com/users/tomtt/events{/privacy}\",\n" +
                "    \"received_events_url\": \"https://api.github.com/users/tomtt/received_events\",\n" +
                "    \"type\": \"User\",\n" +
                "    \"site_admin\": false\n" +
                "  },\n" +
                "  {\n" +
                "    \"login\": \"railsjitsu\",\n" +
                "    \"id\": 32,\n" +
                "    \"avatar_url\": \"https://avatars2.githubusercontent.com/u/32?v=4\",\n" +
                "    \"gravatar_id\": \"\",\n" +
                "    \"url\": \"https://api.github.com/users/railsjitsu\",\n" +
                "    \"html_url\": \"https://github.com/railsjitsu\",\n" +
                "    \"followers_url\": \"https://api.github.com/users/railsjitsu/followers\",\n" +
                "    \"following_url\": \"https://api.github.com/users/railsjitsu/following{/other_user}\",\n" +
                "    \"gists_url\": \"https://api.github.com/users/railsjitsu/gists{/gist_id}\",\n" +
                "    \"starred_url\": \"https://api.github.com/users/railsjitsu/starred{/owner}{/repo}\",\n" +
                "    \"subscriptions_url\": \"https://api.github.com/users/railsjitsu/subscriptions\",\n" +
                "    \"organizations_url\": \"https://api.github.com/users/railsjitsu/orgs\",\n" +
                "    \"repos_url\": \"https://api.github.com/users/railsjitsu/repos\",\n" +
                "    \"events_url\": \"https://api.github.com/users/railsjitsu/events{/privacy}\",\n" +
                "    \"received_events_url\": \"https://api.github.com/users/railsjitsu/received_events\",\n" +
                "    \"type\": \"User\",\n" +
                "    \"site_admin\": false\n" +
                "  },\n" +
                "  {\n" +
                "    \"login\": \"nitay\",\n" +
                "    \"id\": 34,\n" +
                "    \"avatar_url\": \"https://avatars2.githubusercontent.com/u/34?v=4\",\n" +
                "    \"gravatar_id\": \"\",\n" +
                "    \"url\": \"https://api.github.com/users/nitay\",\n" +
                "    \"html_url\": \"https://github.com/nitay\",\n" +
                "    \"followers_url\": \"https://api.github.com/users/nitay/followers\",\n" +
                "    \"following_url\": \"https://api.github.com/users/nitay/following{/other_user}\",\n" +
                "    \"gists_url\": \"https://api.github.com/users/nitay/gists{/gist_id}\",\n" +
                "    \"starred_url\": \"https://api.github.com/users/nitay/starred{/owner}{/repo}\",\n" +
                "    \"subscriptions_url\": \"https://api.github.com/users/nitay/subscriptions\",\n" +
                "    \"organizations_url\": \"https://api.github.com/users/nitay/orgs\",\n" +
                "    \"repos_url\": \"https://api.github.com/users/nitay/repos\",\n" +
                "    \"events_url\": \"https://api.github.com/users/nitay/events{/privacy}\",\n" +
                "    \"received_events_url\": \"https://api.github.com/users/nitay/received_events\",\n" +
                "    \"type\": \"User\",\n" +
                "    \"site_admin\": false\n" +
                "  },\n" +
                "  {\n" +
                "    \"login\": \"kevwil\",\n" +
                "    \"id\": 35,\n" +
                "    \"avatar_url\": \"https://avatars2.githubusercontent.com/u/35?v=4\",\n" +
                "    \"gravatar_id\": \"\",\n" +
                "    \"url\": \"https://api.github.com/users/kevwil\",\n" +
                "    \"html_url\": \"https://github.com/kevwil\",\n" +
                "    \"followers_url\": \"https://api.github.com/users/kevwil/followers\",\n" +
                "    \"following_url\": \"https://api.github.com/users/kevwil/following{/other_user}\",\n" +
                "    \"gists_url\": \"https://api.github.com/users/kevwil/gists{/gist_id}\",\n" +
                "    \"starred_url\": \"https://api.github.com/users/kevwil/starred{/owner}{/repo}\",\n" +
                "    \"subscriptions_url\": \"https://api.github.com/users/kevwil/subscriptions\",\n" +
                "    \"organizations_url\": \"https://api.github.com/users/kevwil/orgs\",\n" +
                "    \"repos_url\": \"https://api.github.com/users/kevwil/repos\",\n" +
                "    \"events_url\": \"https://api.github.com/users/kevwil/events{/privacy}\",\n" +
                "    \"received_events_url\": \"https://api.github.com/users/kevwil/received_events\",\n" +
                "    \"type\": \"User\",\n" +
                "    \"site_admin\": false\n" +
                "  },\n" +
                "  {\n" +
                "    \"login\": \"KirinDave\",\n" +
                "    \"id\": 36,\n" +
                "    \"avatar_url\": \"https://avatars2.githubusercontent.com/u/36?v=4\",\n" +
                "    \"gravatar_id\": \"\",\n" +
                "    \"url\": \"https://api.github.com/users/KirinDave\",\n" +
                "    \"html_url\": \"https://github.com/KirinDave\",\n" +
                "    \"followers_url\": \"https://api.github.com/users/KirinDave/followers\",\n" +
                "    \"following_url\": \"https://api.github.com/users/KirinDave/following{/other_user}\",\n" +
                "    \"gists_url\": \"https://api.github.com/users/KirinDave/gists{/gist_id}\",\n" +
                "    \"starred_url\": \"https://api.github.com/users/KirinDave/starred{/owner}{/repo}\",\n" +
                "    \"subscriptions_url\": \"https://api.github.com/users/KirinDave/subscriptions\",\n" +
                "    \"organizations_url\": \"https://api.github.com/users/KirinDave/orgs\",\n" +
                "    \"repos_url\": \"https://api.github.com/users/KirinDave/repos\",\n" +
                "    \"events_url\": \"https://api.github.com/users/KirinDave/events{/privacy}\",\n" +
                "    \"received_events_url\": \"https://api.github.com/users/KirinDave/received_events\",\n" +
                "    \"type\": \"User\",\n" +
                "    \"site_admin\": false\n" +
                "  },\n" +
                "  {\n" +
                "    \"login\": \"jamesgolick\",\n" +
                "    \"id\": 37,\n" +
                "    \"avatar_url\": \"https://avatars2.githubusercontent.com/u/37?v=4\",\n" +
                "    \"gravatar_id\": \"\",\n" +
                "    \"url\": \"https://api.github.com/users/jamesgolick\",\n" +
                "    \"html_url\": \"https://github.com/jamesgolick\",\n" +
                "    \"followers_url\": \"https://api.github.com/users/jamesgolick/followers\",\n" +
                "    \"following_url\": \"https://api.github.com/users/jamesgolick/following{/other_user}\",\n" +
                "    \"gists_url\": \"https://api.github.com/users/jamesgolick/gists{/gist_id}\",\n" +
                "    \"starred_url\": \"https://api.github.com/users/jamesgolick/starred{/owner}{/repo}\",\n" +
                "    \"subscriptions_url\": \"https://api.github.com/users/jamesgolick/subscriptions\",\n" +
                "    \"organizations_url\": \"https://api.github.com/users/jamesgolick/orgs\",\n" +
                "    \"repos_url\": \"https://api.github.com/users/jamesgolick/repos\",\n" +
                "    \"events_url\": \"https://api.github.com/users/jamesgolick/events{/privacy}\",\n" +
                "    \"received_events_url\": \"https://api.github.com/users/jamesgolick/received_events\",\n" +
                "    \"type\": \"User\",\n" +
                "    \"site_admin\": false\n" +
                "  },\n" +
                "  {\n" +
                "    \"login\": \"atmos\",\n" +
                "    \"id\": 38,\n" +
                "    \"avatar_url\": \"https://avatars3.githubusercontent.com/u/38?v=4\",\n" +
                "    \"gravatar_id\": \"\",\n" +
                "    \"url\": \"https://api.github.com/users/atmos\",\n" +
                "    \"html_url\": \"https://github.com/atmos\",\n" +
                "    \"followers_url\": \"https://api.github.com/users/atmos/followers\",\n" +
                "    \"following_url\": \"https://api.github.com/users/atmos/following{/other_user}\",\n" +
                "    \"gists_url\": \"https://api.github.com/users/atmos/gists{/gist_id}\",\n" +
                "    \"starred_url\": \"https://api.github.com/users/atmos/starred{/owner}{/repo}\",\n" +
                "    \"subscriptions_url\": \"https://api.github.com/users/atmos/subscriptions\",\n" +
                "    \"organizations_url\": \"https://api.github.com/users/atmos/orgs\",\n" +
                "    \"repos_url\": \"https://api.github.com/users/atmos/repos\",\n" +
                "    \"events_url\": \"https://api.github.com/users/atmos/events{/privacy}\",\n" +
                "    \"received_events_url\": \"https://api.github.com/users/atmos/received_events\",\n" +
                "    \"type\": \"User\",\n" +
                "    \"site_admin\": false\n" +
                "  },\n" +
                "  {\n" +
                "    \"login\": \"errfree\",\n" +
                "    \"id\": 44,\n" +
                "    \"avatar_url\": \"https://avatars2.githubusercontent.com/u/44?v=4\",\n" +
                "    \"gravatar_id\": \"\",\n" +
                "    \"url\": \"https://api.github.com/users/errfree\",\n" +
                "    \"html_url\": \"https://github.com/errfree\",\n" +
                "    \"followers_url\": \"https://api.github.com/users/errfree/followers\",\n" +
                "    \"following_url\": \"https://api.github.com/users/errfree/following{/other_user}\",\n" +
                "    \"gists_url\": \"https://api.github.com/users/errfree/gists{/gist_id}\",\n" +
                "    \"starred_url\": \"https://api.github.com/users/errfree/starred{/owner}{/repo}\",\n" +
                "    \"subscriptions_url\": \"https://api.github.com/users/errfree/subscriptions\",\n" +
                "    \"organizations_url\": \"https://api.github.com/users/errfree/orgs\",\n" +
                "    \"repos_url\": \"https://api.github.com/users/errfree/repos\",\n" +
                "    \"events_url\": \"https://api.github.com/users/errfree/events{/privacy}\",\n" +
                "    \"received_events_url\": \"https://api.github.com/users/errfree/received_events\",\n" +
                "    \"type\": \"Organization\",\n" +
                "    \"site_admin\": false\n" +
                "  },\n" +
                "  {\n" +
                "    \"login\": \"mojodna\",\n" +
                "    \"id\": 45,\n" +
                "    \"avatar_url\": \"https://avatars2.githubusercontent.com/u/45?v=4\",\n" +
                "    \"gravatar_id\": \"\",\n" +
                "    \"url\": \"https://api.github.com/users/mojodna\",\n" +
                "    \"html_url\": \"https://github.com/mojodna\",\n" +
                "    \"followers_url\": \"https://api.github.com/users/mojodna/followers\",\n" +
                "    \"following_url\": \"https://api.github.com/users/mojodna/following{/other_user}\",\n" +
                "    \"gists_url\": \"https://api.github.com/users/mojodna/gists{/gist_id}\",\n" +
                "    \"starred_url\": \"https://api.github.com/users/mojodna/starred{/owner}{/repo}\",\n" +
                "    \"subscriptions_url\": \"https://api.github.com/users/mojodna/subscriptions\",\n" +
                "    \"organizations_url\": \"https://api.github.com/users/mojodna/orgs\",\n" +
                "    \"repos_url\": \"https://api.github.com/users/mojodna/repos\",\n" +
                "    \"events_url\": \"https://api.github.com/users/mojodna/events{/privacy}\",\n" +
                "    \"received_events_url\": \"https://api.github.com/users/mojodna/received_events\",\n" +
                "    \"type\": \"User\",\n" +
                "    \"site_admin\": false\n" +
                "  },\n" +
                "  {\n" +
                "    \"login\": \"bmizerany\",\n" +
                "    \"id\": 46,\n" +
                "    \"avatar_url\": \"https://avatars2.githubusercontent.com/u/46?v=4\",\n" +
                "    \"gravatar_id\": \"\",\n" +
                "    \"url\": \"https://api.github.com/users/bmizerany\",\n" +
                "    \"html_url\": \"https://github.com/bmizerany\",\n" +
                "    \"followers_url\": \"https://api.github.com/users/bmizerany/followers\",\n" +
                "    \"following_url\": \"https://api.github.com/users/bmizerany/following{/other_user}\",\n" +
                "    \"gists_url\": \"https://api.github.com/users/bmizerany/gists{/gist_id}\",\n" +
                "    \"starred_url\": \"https://api.github.com/users/bmizerany/starred{/owner}{/repo}\",\n" +
                "    \"subscriptions_url\": \"https://api.github.com/users/bmizerany/subscriptions\",\n" +
                "    \"organizations_url\": \"https://api.github.com/users/bmizerany/orgs\",\n" +
                "    \"repos_url\": \"https://api.github.com/users/bmizerany/repos\",\n" +
                "    \"events_url\": \"https://api.github.com/users/bmizerany/events{/privacy}\",\n" +
                "    \"received_events_url\": \"https://api.github.com/users/bmizerany/received_events\",\n" +
                "    \"type\": \"User\",\n" +
                "    \"site_admin\": false\n" +
                "  }\n" +
                "]";

        BaseNameResponse response = gson.fromJson(answer, BaseNameResponse.class);

        assertNotNull(response);
        assertEquals(response.get(0).getLoginUser(), "mojombo");
    }

    @Test
    public void testGetExtraUser() {
        String answer = "{\n" +
                "  \"login\": \"mojombo\",\n" +
                "  \"id\": 1,\n" +
                "  \"avatar_url\": \"https://avatars0.githubusercontent.com/u/1?v=4\",\n" +
                "  \"gravatar_id\": \"\",\n" +
                "  \"url\": \"https://api.github.com/users/mojombo\",\n" +
                "  \"html_url\": \"https://github.com/mojombo\",\n" +
                "  \"followers_url\": \"https://api.github.com/users/mojombo/followers\",\n" +
                "  \"following_url\": \"https://api.github.com/users/mojombo/following{/other_user}\",\n" +
                "  \"gists_url\": \"https://api.github.com/users/mojombo/gists{/gist_id}\",\n" +
                "  \"starred_url\": \"https://api.github.com/users/mojombo/starred{/owner}{/repo}\",\n" +
                "  \"subscriptions_url\": \"https://api.github.com/users/mojombo/subscriptions\",\n" +
                "  \"organizations_url\": \"https://api.github.com/users/mojombo/orgs\",\n" +
                "  \"repos_url\": \"https://api.github.com/users/mojombo/repos\",\n" +
                "  \"events_url\": \"https://api.github.com/users/mojombo/events{/privacy}\",\n" +
                "  \"received_events_url\": \"https://api.github.com/users/mojombo/received_events\",\n" +
                "  \"type\": \"User\",\n" +
                "  \"site_admin\": false,\n" +
                "  \"name\": \"Tom Preston-Werner\",\n" +
                "  \"company\": null,\n" +
                "  \"blog\": \"http://tom.preston-werner.com\",\n" +
                "  \"location\": \"San Francisco\",\n" +
                "  \"email\": null,\n" +
                "  \"hireable\": null,\n" +
                "  \"bio\": null,\n" +
                "  \"public_repos\": 61,\n" +
                "  \"public_gists\": 62,\n" +
                "  \"followers\": 20352,\n" +
                "  \"following\": 11,\n" +
                "  \"created_at\": \"2007-10-20T05:24:19Z\",\n" +
                "  \"updated_at\": \"2017-07-29T10:00:13Z\"\n" +
                "}";

        User response = gson.fromJson(answer, User.class);

        assertNotNull(response);
        assertEquals(response.getLoginUser(), "mojombo");
    }
}
