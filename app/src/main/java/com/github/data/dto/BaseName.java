package com.github.data.dto;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

import lombok.Getter;

@Getter
public class BaseName implements Serializable {
    @SerializedName("full_name")
    private String fullNameRepo;
    @SerializedName("login")
    private String loginUser;
}
