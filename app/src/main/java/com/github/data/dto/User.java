package com.github.data.dto;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

import lombok.Getter;

@Getter
public class User extends BaseName implements Serializable {
    @SerializedName("avatar_url")
    private String avatarUrl;
    @SerializedName("followers")
    private int followers;
    @SerializedName("following")
    private int following;
}
