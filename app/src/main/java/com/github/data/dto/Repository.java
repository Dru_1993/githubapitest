package com.github.data.dto;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

import lombok.Getter;

@Getter
public class Repository extends BaseName implements Serializable {
    @SerializedName("description")
    private String description;
    @SerializedName("stargazers_count")
    private int stargazers;
    @SerializedName("forks_count")
    private int forks;
}
