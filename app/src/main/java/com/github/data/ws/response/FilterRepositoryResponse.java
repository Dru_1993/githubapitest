package com.github.data.ws.response;

public class FilterRepositoryResponse extends BaseFilterResponse<RepositoryResponse> {

    public RepositoryResponse getData() {
        return data;
    }
}
