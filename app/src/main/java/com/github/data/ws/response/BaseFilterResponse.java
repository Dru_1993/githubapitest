package com.github.data.ws.response;

import com.google.gson.annotations.SerializedName;

public class BaseFilterResponse<T> {
    @SerializedName("items")
    protected T data;
}
