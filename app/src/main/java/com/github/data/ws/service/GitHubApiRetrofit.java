package com.github.data.ws.service;

import android.content.Context;

import com.github.data.ws.API;
import com.github.domain.utility.NetworkUtility;
import com.jakewharton.retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;

import java.util.Locale;

import okhttp3.Cache;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class GitHubApiRetrofit {
    private static final int CACHE_SIZE = 10 * 1024 * 1024;
    private static final int TIME_ACTUAL_CONTENT = 60;
    private static final int TIME_CACHE = 60 * 60 * 24 * 7;
    private static final String CACHE_CONTROL = "Cache-Control";
    private static final String AUTHORIZATION = "Authorization";
    private static final String FORMAT_TIME_ACTUAL_CONTENT = "public, max-age=%d";
    private static final String FORMAT_TIME_CACHE = "public, only-if-cached, max-stale=%d";
    private static final String BASIC_AUTH = "Basic ZHJ1MTk5Mzp2ZXJrZWV2XzIx";
    private Retrofit retrofit;

    public GitHubApiRetrofit(Context context) {
        retrofit = new Retrofit.Builder()
                .baseUrl(getServerUrl())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .client(buildCache(context))
                .build();
    }

    private OkHttpClient buildCache(Context context) {
        Cache cache = new Cache(context.getCacheDir(), CACHE_SIZE);
        return new OkHttpClient()
                .newBuilder()
                .cache(cache)
                .addInterceptor(getAuthInterceptor())
                .addInterceptor(getRewriteCacheInterceptor(context))
                .build();
    }

    private Interceptor getRewriteCacheInterceptor(Context context) {
        return chain -> {
            String param;
            if (NetworkUtility.isNetworkActive(context)) {
                param = String.format(Locale.getDefault(), FORMAT_TIME_ACTUAL_CONTENT, TIME_ACTUAL_CONTENT);
            } else {
                param = String.format(Locale.getDefault(), FORMAT_TIME_CACHE, TIME_CACHE);
            }

            Request request = chain.request();
            request = request.newBuilder().header(CACHE_CONTROL, param).build();
            return chain.proceed(request);
        };
    }

    private Interceptor getAuthInterceptor() {
        return chain -> {
            Request request = chain.request();
            request = request.newBuilder().header(AUTHORIZATION, BASIC_AUTH).build();
            return chain.proceed(request);
        };
    }

    public API getService() {
        return retrofit.create(API.class);
    }

    private String getServerUrl() {
        return "https://api.github.com";
    }
}
