package com.github.data.ws;

import com.github.data.dto.Repository;
import com.github.data.dto.User;
import com.github.data.ws.response.BaseNameResponse;
import com.github.data.ws.response.FilterRepositoryResponse;
import com.github.data.ws.response.FilterUserResponse;

import io.reactivex.Observable;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface API {

    @GET("/users")
    Observable<BaseNameResponse> getUsers();

    @GET("/repositories")
    Observable<BaseNameResponse> getRepositories();

    @GET("/search/users")
    Observable<FilterUserResponse> getFilterUsers(@Query("q") String name);

    @GET("/search/repositories")
    Observable<FilterRepositoryResponse> getFilterRepositories(@Query("q") String name);

    @GET("/users/{name}")
    Observable<User> getExtrasUser(@Path("name") String name);

    @GET("/repos/{name}")
    Observable<Repository> getExtrasRepository(@Path(value = "name", encoded = true) String name);
}
