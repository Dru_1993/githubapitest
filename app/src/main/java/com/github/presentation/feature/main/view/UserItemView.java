package com.github.presentation.feature.main.view;


import android.annotation.TargetApi;
import android.content.Context;
import android.os.Build;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.github.data.dto.User;
import com.github.test.R;
import com.nostra13.universalimageloader.core.ImageLoader;

public class UserItemView extends LinearLayout {
    private ImageView avatarImageView;
    private TextView nameTextView, followingTextView, followersTextView;

    public UserItemView(Context context) {
        super(context);
    }

    public UserItemView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
    }

    public UserItemView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public UserItemView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
    }

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();

        avatarImageView = (ImageView) findViewById(R.id.avatar);
        nameTextView = (TextView) findViewById(R.id.name_text);
        followingTextView = (TextView) findViewById(R.id.following_text);
        followersTextView = (TextView) findViewById(R.id.followers_text);
    }

    public void populate(User user) {
        ImageLoader.getInstance().displayImage(user.getAvatarUrl(), avatarImageView);
        nameTextView.setText(user.getLoginUser());
        followersTextView.setText(String.format(getResources().getString(R.string.format_followers), user.getFollowers()));
        followingTextView.setText(String.format(getResources().getString(R.string.format_following), user.getFollowing()));
    }
}
