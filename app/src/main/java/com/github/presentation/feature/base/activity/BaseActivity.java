package com.github.presentation.feature.base.activity;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;

import com.github.domain.controller.GitHubApiController;

public class BaseActivity extends AppCompatActivity {
    protected GitHubApiController gitHubApiController;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        gitHubApiController = new GitHubApiController(this);
    }

    @Override
    protected void onStart() {
        super.onStart();
        gitHubApiController.attach(this);
    }

    @Override
    protected void onStop() {
        super.onStop();
        gitHubApiController.detach();
    }

    public void setTitleBar(int resId, boolean isBackButton) {
        if (getSupportActionBar() != null) {
            getSupportActionBar().setTitle(getString(resId));
            getSupportActionBar().setDisplayHomeAsUpEnabled(isBackButton);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
