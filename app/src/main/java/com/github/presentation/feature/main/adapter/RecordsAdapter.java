package com.github.presentation.feature.main.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.github.data.dto.Repository;
import com.github.data.dto.User;
import com.github.presentation.feature.base.adapter.RecyclerListAdapter;
import com.github.presentation.feature.main.view.RepositoryItemView;
import com.github.presentation.feature.main.view.UserItemView;
import com.github.test.R;

import java.util.List;

public class RecordsAdapter extends RecyclerListAdapter<Object, RecyclerView.ViewHolder> {
    private static final int ITEM_USER = 0;
    private static final int ITEM_REPOSITORY = 1;

    @Override
    public int getItemViewType(int position) {
        if (getItem(position) instanceof User) {
            return ITEM_USER;
        }
        return ITEM_REPOSITORY;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == ITEM_USER) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_user, parent, false);
            return new UserViewHolder(view);
        }
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_repository, parent, false);
        return new RepositoryViewHolder(view);
    }

    @Override
    protected void onBindCustomViewHolder(RecyclerView.ViewHolder holder, int position) {
        if (getItemViewType(position) == ITEM_USER) {
            ((UserViewHolder) holder).populate((User) getItem(position));
        } else {
            ((RepositoryViewHolder) holder).populate((Repository) getItem(position));
        }
    }

    public List<Object> getData() {
        return data;
    }

    private class UserViewHolder extends RecyclerView.ViewHolder {
        private UserItemView itemView;

        UserViewHolder(View itemView) {
            super(itemView);
            this.itemView = (UserItemView) itemView;
        }

        void populate(final User user) {
            itemView.populate(user);
        }
    }

    private class RepositoryViewHolder extends RecyclerView.ViewHolder {
        private RepositoryItemView itemView;

        RepositoryViewHolder(View itemView) {
            super(itemView);
            this.itemView = (RepositoryItemView) itemView;
        }

        void populate(final Repository repository) {
            itemView.populate(repository);
        }
    }
}
