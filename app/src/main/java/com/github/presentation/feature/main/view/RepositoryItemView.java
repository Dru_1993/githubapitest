package com.github.presentation.feature.main.view;

import android.annotation.TargetApi;
import android.content.Context;
import android.os.Build;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.github.data.dto.Repository;
import com.github.test.R;

public class RepositoryItemView extends LinearLayout {
    private TextView nameRepoTextView, descRepoTextView, starRepoTextView, forksRepoTextView;

    public RepositoryItemView(Context context) {
        super(context);
    }

    public RepositoryItemView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
    }

    public RepositoryItemView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public RepositoryItemView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
    }

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();

        nameRepoTextView = (TextView) findViewById(R.id.name_text_repo);
        descRepoTextView = (TextView) findViewById(R.id.description_repo);
        starRepoTextView = (TextView) findViewById(R.id.star_repo);
        forksRepoTextView = (TextView) findViewById(R.id.fork_repo);
    }

    public void populate(Repository repository) {
        nameRepoTextView.setText(repository.getFullNameRepo());
        descRepoTextView.setText(repository.getDescription());
        starRepoTextView.setText(String.format(getResources().getString(R.string.format_stars), repository.getStargazers()));
        forksRepoTextView.setText(String.format(getResources().getString(R.string.format_forks), repository.getForks()));
    }
}
