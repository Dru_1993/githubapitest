package com.github.presentation.feature.main.activity;

import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.github.data.dto.Repository;
import com.github.data.dto.User;
import com.github.data.ws.response.BaseNameResponse;
import com.github.data.ws.response.FilterRepositoryResponse;
import com.github.data.ws.response.FilterUserResponse;
import com.github.domain.controller.ILoadListener;
import com.github.presentation.feature.base.activity.BaseActivity;
import com.github.presentation.feature.main.adapter.RecordsAdapter;
import com.github.test.R;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends BaseActivity {
    private static final String EXTRA_KEY_USER = "EXTRA_KEY_USER";
    private static final String EXTRA_KEY_REPO = "EXTRA_KEY_REPO";
    private SwipeRefreshLayout swipeRefresh;
    private View progressView;
    private TextView errorTextView;
    private RecordsAdapter adapter;
    private List<User> dataUser = new ArrayList<>();
    private List<Repository> dataRepository = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        setTitleBar(R.string.app_name, false);

        RecyclerView listRecords = (RecyclerView) findViewById(R.id.list_records);
        listRecords.setLayoutManager(new LinearLayoutManager(this));
        adapter = new RecordsAdapter();
        listRecords.setAdapter(adapter);

        if (savedInstanceState != null) {
            processResumeData(savedInstanceState);
        }

        swipeRefresh = (SwipeRefreshLayout) findViewById(R.id.swipeRefresh);
        swipeRefresh.setOnRefreshListener(this::loadUsers);

        errorTextView = (TextView) findViewById(R.id.error_text);

        progressView = findViewById(R.id.progress_view);
    }

    @Override
    protected void onStart() {
        super.onStart();
        if (adapter.getItemCount() == 0) {
            loadUsers();
        }
    }

    @SuppressWarnings("unchecked")
    private void processResumeData(Bundle savedInstanceState) {
        dataUser = (List<User>) savedInstanceState.getSerializable(EXTRA_KEY_USER);
        dataRepository = (List<Repository>) savedInstanceState.getSerializable(EXTRA_KEY_REPO);
        processDataAdapter();
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putSerializable(EXTRA_KEY_USER, new ArrayList<>(dataUser));
        outState.putSerializable(EXTRA_KEY_REPO, new ArrayList<>(dataRepository));
    }

    public void loadUsers() {
        dataRepository.clear();
        dataUser.clear();
        showErrorView(false);
        showProgress();
        gitHubApiController.getUsers(new ILoadListener<BaseNameResponse>() {

            @Override
            public void onFailure(Throwable throwable) {
                hideProgress();
                showErrorView(true);
            }

            @Override
            public void onSuccess(BaseNameResponse answer) {
                if (!answer.isEmpty()) {
                    processUsers(answer);
                }
            }
        });
    }

    private void processUsers(BaseNameResponse answer) {
        for (int i = 0; i < answer.size(); i++) {
            loadExtraUser(answer.get(i).getLoginUser(), i == answer.size() - 1, false, null);
        }
    }

    private void checkLoadRepositories(String query, boolean isFilter) {
        if (!isFilter) {
            loadRepositories();
        } else {
            loadFilterRepository(query);
        }
    }

    private void loadRepositories() {
        gitHubApiController.getRepositories(new ILoadListener<BaseNameResponse>() {

            @Override
            public void onFailure(Throwable throwable) {
                hideProgress();
                showErrorView(true);
            }

            @Override
            public void onSuccess(BaseNameResponse answer) {
                if (!answer.isEmpty()) {
                    processRepositories(answer);
                }
            }
        });
    }

    private void processRepositories(BaseNameResponse answer) {
        for (int i = 0; i < answer.size(); i++) {
            loadExtraRepository(answer.get(i).getFullNameRepo(), i == answer.size() - 1);
        }
    }

    private void loadExtraRepository(String name, final boolean isLastQuery) {
        gitHubApiController.getExtraRepository(name, new ILoadListener<Repository>() {

            @Override
            public void onFailure(Throwable throwable) {
                hideProgress();
                showErrorView(true);
            }

            @Override
            public void onSuccess(Repository answer) {
                dataRepository.add(answer);
                if (isLastQuery) {
                    hideProgress();
                    processDataAdapter();
                }
            }
        });
    }

    public void showProgress() {
        progressView.setVisibility(View.VISIBLE);
    }

    public void hideProgress() {
        if (swipeRefresh.isRefreshing()) {
            swipeRefresh.setRefreshing(false);
        }
        progressView.setVisibility(View.GONE);
    }

    public void showErrorView(boolean isShow) {
        swipeRefresh.setVisibility(isShow ? View.GONE : View.VISIBLE);
        errorTextView.setVisibility(isShow ? View.VISIBLE : View.GONE);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.search_menu, menu);
        final MenuItem searchMenuItem = menu.findItem(R.id.action_search);

        final SearchView searchView = (SearchView) searchMenuItem.getActionView();
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {

            @Override
            public boolean onQueryTextSubmit(String query) {
                loadFilterUsers(query);
                if (!searchView.isIconified()) {
                    searchView.setIconified(true);
                }
                searchMenuItem.collapseActionView();
                return false;
            }

            @Override
            public boolean onQueryTextChange(String query) {
                return false;
            }
        });
        return true;
    }

    private void loadFilterUsers(final String query) {
        dataRepository.clear();
        dataUser.clear();
        showProgress();
        showErrorView(false);
        gitHubApiController.getFilterUsers(query, new ILoadListener<FilterUserResponse>() {

            @Override
            public void onFailure(Throwable throwable) {
                hideProgress();
                showErrorView(true);
            }

            @Override
            public void onSuccess(FilterUserResponse answer) {
                if (!answer.getData().isEmpty()) {
                    processFilterUsers(query, answer.getData());
                }
            }
        });
    }

    private void processFilterUsers(String query, BaseNameResponse answer) {
        for (int i = 0; i < answer.size(); i++) {
            loadExtraUser(answer.get(i).getLoginUser(), i == answer.size() - 1, true, query);
        }
    }

    private void loadFilterRepository(String query) {
        gitHubApiController.getFilterRepositories(query, new ILoadListener<FilterRepositoryResponse>() {

            @Override
            public void onFailure(Throwable throwable) {
                hideProgress();
                showErrorView(true);
            }

            @Override
            public void onSuccess(FilterRepositoryResponse answer) {
                hideProgress();
                dataRepository.addAll(answer.getData());
                processDataAdapter();
            }
        });
    }

    private void loadExtraUser(String name, final boolean isLastQuery, final boolean isFilter, final String query) {
        gitHubApiController.getExtrasUser(name, new ILoadListener<User>() {

            @Override
            public void onFailure(Throwable throwable) {
                hideProgress();
                showErrorView(true);
            }

            @Override
            public void onSuccess(User answer) {
                dataUser.add(answer);
                if (isLastQuery) {
                    checkLoadRepositories(query, isFilter);
                }
            }
        });
    }

    private void processDataAdapter() {
        int size;
        List<Object> dataAdapter = new ArrayList<>();
        boolean isCounterByUser = dataUser.size() < dataRepository.size();

        if (isCounterByUser) {
            size = dataUser.size();
        } else {
            size = dataRepository.size();
        }

        for (int i = 0; i < size; i++) {
            dataAdapter.add(dataUser.get(i));
            dataAdapter.add(dataRepository.get(i));
        }

        if (isCounterByUser) {
            dataAdapter.addAll(dataRepository.subList(size, dataRepository.size()));
        } else {
            dataAdapter.addAll(dataUser.subList(size, dataUser.size()));
        }

        adapter.clear();
        adapter.addAll(dataAdapter);
    }
}
