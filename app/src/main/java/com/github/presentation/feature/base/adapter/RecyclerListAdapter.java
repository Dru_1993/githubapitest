package com.github.presentation.feature.base.adapter;


import android.support.v7.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

public abstract class RecyclerListAdapter<T, VH extends RecyclerView.ViewHolder> extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    protected List<T> data;

    public RecyclerListAdapter() {
        this.data = new ArrayList<>();
    }

    public RecyclerListAdapter(final List<T> data) {
        this.data = data;
    }

    public boolean add(final T object) {
        boolean isModified = data.add(object);
        if (isModified) {
            notifyItemInserted(getItemCount());
        }
        return isModified;
    }


    public void clear() {
        final int size = getItemCount();
        data.clear();
        notifyItemRangeRemoved(0, size);
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public T getItem(final int position) {
        return data.get(position);
    }

    public int getPosition(final T item) {
        return data.indexOf(item);
    }

    public boolean addAll(List<T> historiesStates) {
        int positionStart = data.size() + 1;
        boolean isModified = data.addAll(historiesStates);
        if (isModified) {
            notifyItemRangeInserted(positionStart, historiesStates.size());
        }
        return isModified;
    }

    @Override
    public final void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        onBindCustomViewHolder((VH) holder, position);
    }

    protected abstract void onBindCustomViewHolder(VH holder, int position);
}
