package com.github.domain.controller;

import android.content.Context;

import com.github.data.dto.Repository;
import com.github.data.dto.User;
import com.github.data.ws.response.BaseNameResponse;
import com.github.data.ws.response.FilterRepositoryResponse;
import com.github.data.ws.response.FilterUserResponse;

public class GitHubApiController extends BaseController {

    public GitHubApiController(Context context) {
        super(context);
    }

    public void getUsers(ILoadListener<BaseNameResponse> listener) {
        execute(apiService.getUsers(), listener);
    }

    public void getRepositories(ILoadListener<BaseNameResponse> listener) {
        execute(apiService.getRepositories(), listener);
    }

    public void getFilterUsers(String searchText, ILoadListener<FilterUserResponse> listener) {
        execute(apiService.getFilterUsers(searchText), listener);
    }

    public void getFilterRepositories(String searchText, ILoadListener<FilterRepositoryResponse> listener) {
        execute(apiService.getFilterRepositories(searchText), listener);
    }

    public void getExtrasUser(String name, ILoadListener<User> listener) {
        execute(apiService.getExtrasUser(name), listener);
    }

    public void getExtraRepository(String name, ILoadListener<Repository> listener) {
        execute(apiService.getExtrasRepository(name), listener);
    }
}
