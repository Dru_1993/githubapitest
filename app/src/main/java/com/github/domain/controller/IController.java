package com.github.domain.controller;

import android.content.Context;

import io.reactivex.Observable;

public interface IController {

    void attach(Context context);

    void detach();

    <T> void execute(Observable<T> observable, ILoadListener<T> listener);
}
