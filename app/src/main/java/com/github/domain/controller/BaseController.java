package com.github.domain.controller;

import android.content.Context;

import com.github.data.ws.API;
import com.github.data.ws.service.GitHubApiRetrofit;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;

public class BaseController implements IController {
    private CompositeDisposable disposable;
    protected API apiService;

    protected BaseController(Context context) {
        apiService = new GitHubApiRetrofit(context).getService();
    }

    @Override
    public void attach(Context context) {
        disposable = new CompositeDisposable();
    }

    @Override
    public void detach() {
        disposable.dispose();
    }

    @Override
    public <T> void execute(Observable<T> observable, ILoadListener<T> listener) {
        disposable.add(observable
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(listener::onSuccess, listener::onFailure));
    }
}
