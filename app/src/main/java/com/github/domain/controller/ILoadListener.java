package com.github.domain.controller;

public interface ILoadListener<T> {

    void onSuccess(T answer);

    void onFailure(Throwable throwable);
}
